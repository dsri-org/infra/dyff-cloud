# SPDX-FileCopyrightText: 2024 UL Research Institutes
# SPDX-License-Identifier: Apache-2.0

provider "google" {
  credentials = file(var.google_cloud_service_account_file)
  project     = var.project

  default_labels = local.default_tags
}

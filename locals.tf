# SPDX-FileCopyrightText: 2024 UL Research Institutes
# SPDX-License-Identifier: Apache-2.0

locals {
  name       = "${var.environment}-${local.deployment}"
  region     = "us-central1"
  deployment = "dyff-cloud"

  default_tags = {
    deployment  = local.deployment
    environment = var.environment
  }
}

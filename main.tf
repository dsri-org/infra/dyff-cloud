# SPDX-FileCopyrightText: 2024 UL Research Institutes
# SPDX-License-Identifier: Apache-2.0

# create the vpc
resource "google_compute_network" "dyff" {
  name                            = local.name
  delete_default_routes_on_create = false
  auto_create_subnetworks         = false
  routing_mode                    = "REGIONAL"
}

# create private subnet and assign addresses to all the zones in the region
resource "google_compute_subnetwork" "dyff" {
  name          = "${local.name}-subnetwork"
  ip_cidr_range = "10.0.0.0/16"
  region        = local.region
  network       = google_compute_network.dyff.id

  private_ip_google_access = true

  secondary_ip_range {
    range_name    = "pod-ranges"
    ip_cidr_range = "192.168.0.0/18"
  }

  secondary_ip_range {
    range_name    = "services-range"
    ip_cidr_range = "192.168.64.0/18"
  }
}

resource "google_compute_router" "dyff" {
  name    = "${local.name}-router"
  region  = google_compute_subnetwork.dyff.region
  network = google_compute_network.dyff.id
}

resource "google_compute_router_nat" "dyff" {
  name                               = "${local.name}-router-nat"
  router                             = google_compute_router.dyff.name
  region                             = google_compute_router.dyff.region
  nat_ip_allocate_option             = "AUTO_ONLY"
  source_subnetwork_ip_ranges_to_nat = "LIST_OF_SUBNETWORKS"

  subnetwork {
    name                    = "${local.name}-subnetwork"
    source_ip_ranges_to_nat = ["ALL_IP_RANGES"]
  }
}

resource "google_service_account" "cluster_node" {
  account_id   = "tf-${local.deployment}-cluster-node"
  display_name = "tf-${local.deployment}-cluster-node"
}

# https://cloud.google.com/kubernetes-engine/docs/how-to/service-accounts#default-gke-service-agent
# https://cloud.google.com/kubernetes-engine/docs/how-to/hardening-your-cluster#use_least_privilege_sa
resource "google_project_iam_member" "cluster_node" {
  for_each = toset([
    "roles/autoscaling.metricsWriter",
    "roles/logging.logWriter",
    "roles/monitoring.metricWriter",
    "roles/monitoring.viewer",
    "roles/stackdriver.resourceMetadata.writer",
  ])
  project = var.project
  role    = each.key
  member  = "serviceAccount:${google_service_account.cluster_node.email}"
}

# Serial port logging is definitely required by GKE Autopilot for normal
# operation:
#
# > "If serial port logging is disabled, Autopilot can't provision nodes to run
# > your workloads."
#
# https://cloud.google.com/kubernetes-engine/docs/troubleshooting/autopilot-clusters#scale-up-failed-serial-port-logging
resource "google_compute_project_metadata_item" "enable_serial_port_logging" {
  key   = "serial-port-logging-enable"
  value = "true"
}

resource "google_container_cluster" "dyff" {
  name     = local.name
  location = google_compute_router.dyff.region
  project  = var.project

  network    = google_compute_network.dyff.id
  subnetwork = google_compute_subnetwork.dyff.id

  deletion_protection = false
  enable_autopilot    = true

  addons_config {
    gke_backup_agent_config {
      enabled = true
    }
  }

  cluster_autoscaling {
    auto_provisioning_defaults {
      management {
        auto_upgrade = true
        auto_repair  = true
      }

      service_account = google_service_account.cluster_node.email
    }
  }

  cost_management_config {
    enabled = true
  }

  ip_allocation_policy {
    cluster_secondary_range_name  = "pod-ranges"
    services_secondary_range_name = "services-range"
  }

  logging_config {
    enable_components = [
      "SYSTEM_COMPONENTS",
      "WORKLOADS",
    ]
  }

  min_master_version = "1.28"

  monitoring_config {
    enable_components = [
      "SYSTEM_COMPONENTS",
      "APISERVER",
      "SCHEDULER",
      "CONTROLLER_MANAGER",
      "STORAGE",
      "HPA",
      "DAEMONSET",
      "DEPLOYMENT",
      "STATEFULSET",
    ]

    managed_prometheus {
      enabled = true
    }
  }

  private_cluster_config {
    enable_private_endpoint = false
    enable_private_nodes    = true
  }

  release_channel {
    channel = "REGULAR"
  }

  resource_labels = local.default_tags

  lifecycle {
    prevent_destroy = true
  }
}
